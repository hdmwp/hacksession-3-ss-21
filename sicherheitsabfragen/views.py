from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

from . import models

def index(request):
    context = {
        "dinge": models.Ding.objects.all()
            }
    return render(request, "sicherheitsabfragen/index.html", context)


def detail(request, ding_id):
    ding = get_object_or_404(models.Ding, pk=ding_id)
    return render(request, "sicherheitsabfragen/detail.html", {"ding": ding})

def sicherheitsabfrage(request, text, id, yes_view, no_view):
        context = {}
        context["zu_loeschen_text"] = text
        context["zu_loeschen_id"] = id
        context["yes_view"] = yes_view
        context["no_view"] = no_view
        return render(request, "sicherheitsabfragen/abfrage.html", context)



def loeschen(request, ding_id):
    ding = get_object_or_404(models.Ding, pk=ding_id)
    if "confirmed" in request.GET:
        ding.delete()
        return redirect("index")
    else:
        return sicherheitsabfrage(request, ding.name, ding.id, "loeschen", "detail")

