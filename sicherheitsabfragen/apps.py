from django.apps import AppConfig


class SicherheitsabfragenConfig(AppConfig):
    name = 'sicherheitsabfragen'
