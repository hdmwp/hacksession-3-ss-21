from django.apps import AppConfig


class TodolisteConfig(AppConfig):
    name = 'todoliste'
