"""hacksession3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    context = {}
    context["links"] = [
            {"url": "admin", "name": "Admin-Oberfläche"},
            {"url": "sicherheitsabfragen", "name": "Test der Sicherheitsabfragen"},
            {"url": "todoliste", "name": "Todo-Liste"},
            ]
    return render(request, "hacksession3/index.html", context)

urlpatterns = [
    path('', index, name="serverindex"), 
    path('admin/', admin.site.urls),
    path('sicherheitsabfragen/', include("sicherheitsabfragen.urls")),
]
